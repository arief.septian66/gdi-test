part of '../cubits.dart';

abstract class ProjectState extends Equatable {
  const ProjectState();

  @override
  List<Object> get props => [];
}

class ProjectInitial extends ProjectState {}

class ProjectFailed extends ProjectState {
  final RequestStatus requestStatus;

  const ProjectFailed(this.requestStatus);

  @override
  List<Object> get props => [requestStatus];
}

class ProjectLoading extends ProjectState {}

class DetailProjectLoaded extends ProjectState {
  final DetailProjectModel projectDetailData;
  const DetailProjectLoaded(this.projectDetailData);

  @override
  List<Object> get props => [projectDetailData];
}

class ProjectPreviewLoaded extends ProjectState {
  final List<PreviewProjectModel> listDataProject;

  const ProjectPreviewLoaded(this.listDataProject);

  @override
  List<Object> get props => [listDataProject];
}
