part of '../cubits.dart';

class ProjectCubit extends Cubit<ProjectState> {
  ProjectCubit() : super(ProjectInitial());

  void loadProjectPreview() {
    emit(ProjectLoading());
    ProjectAPI.listingPreviewProject().then((value) {
      if (value.status == RequestStatus.success_request) {
        emit(ProjectPreviewLoaded(value.data!));
      } else {
        emit(ProjectFailed(value.status!));
      }
    });
  }

  void loadDetailProject({required String id}) {
    emit(ProjectLoading());
    ProjectAPI.detailProject(id: id).then((value) {
      if (value.status == RequestStatus.success_request) {
        emit(DetailProjectLoaded(value.data!));
      } else {
        emit(ProjectFailed(value.status!));
      }
    });
  }
}
