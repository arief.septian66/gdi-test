import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gdi_test/model/models.dart';
import 'package:gdi_test/service/services.dart';
import 'package:gdi_test/shared/shared.dart';

part 'project_cubit/project_state.dart';
part 'project_cubit/project_cubit.dart';
