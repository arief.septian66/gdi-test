part of 'models.dart';

class ApiReturnValue<T> {
  T? data;
  RequestStatus? status;

  ApiReturnValue({this.data, this.status});

  static Future<ApiReturnValue<dynamic>?> httpRequest({
    required http.MultipartRequest request,
  }) async {
    ApiReturnValue<dynamic>? returnValue;

    try {
      final streamSend = await request.send();
      final response = await http.Response.fromStream(streamSend);
      var data = json.decode(response.body);

      print(data);

      if (response.statusCode != 200) {
        returnValue =
            ApiReturnValue(data: data, status: RequestStatus.failed_request);
      } else {
        returnValue =
            ApiReturnValue(status: RequestStatus.success_request, data: data);
      }
    } on SocketException {
      returnValue =
          ApiReturnValue(data: null, status: RequestStatus.internet_error);
    } catch (e) {
      returnValue =
          ApiReturnValue(status: RequestStatus.failed_parsing, data: null);
    }

    return returnValue;
  }
}
