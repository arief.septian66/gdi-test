import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:gdi_test/shared/shared.dart';

part 'http_helper_model.dart';
part 'project_model/preview_project_model.dart';
part 'project_model/detail_project_model.dart';
part 'project_model/developer_model.dart';
