part of '../models.dart';

class DetailProjectModel {
  late PreviewProjectModel previewProjectModel;
  late DeveloperModel developerModel;
  late List<PropertyDetail> propertyDetail = [];
  late List<PropertyProgress> propertyProgress = [];
  late List<Uint8List> listImages = [];
  late String videosUrl;
  late int bagiHasilMonth;
  late int profitPerYear;
  late bool isSellable;
  late String description;
  late String addressFull;
  late String latitude;
  late String longitude;

  DetailProjectModel(
      {required this.previewProjectModel,
      required this.developerModel,
      required this.propertyDetail,
      required this.listImages,
      required this.propertyProgress,
      required this.videosUrl,
      required this.bagiHasilMonth,
      required this.profitPerYear,
      required this.isSellable,
      required this.description,
      required this.addressFull,
      required this.latitude,
      required this.longitude});

  DetailProjectModel.fromJson(Map<String, dynamic> jsonMap) {
    this.previewProjectModel = PreviewProjectModel.fromJsonToDetail(jsonMap);
    final projectDetailData = jsonMap['project_detail'];
    this.developerModel =
        DeveloperModel.fromJson(projectDetailData['developer']);

    for (var i = 0; i < projectDetailData['property_detail'].length; i++) {
      this.propertyDetail.add(
          PropertyDetail.fromJson(projectDetailData['property_detail'][i]));
    }

    for (var i = 0; i < projectDetailData['progress'].length; i++) {
      this
          .propertyProgress
          .add(PropertyProgress.fromJson(projectDetailData['progress'][i]));
    }

    for (var i = 0; i < projectDetailData['images'].length; i++) {
      this
          .listImages
          .add(base64.decode(projectDetailData['images'][i]['data']));
    }

    this.videosUrl = projectDetailData['videos_url'];
    this.bagiHasilMonth = int.parse(projectDetailData['bagi_hasil_month']);
    this.profitPerYear = int.parse(projectDetailData['profit_per_year']);
    this.isSellable =
        int.parse(projectDetailData['is_sellable']) == 1 ? true : false;
    this.description = projectDetailData['description'];
    this.addressFull = projectDetailData['address_full'];
    this.latitude = projectDetailData['latitude'];
    this.longitude = projectDetailData['longitude'];
  }
}

class PropertyDetail {
  late int id;
  late String title;
  late String desc;

  PropertyDetail({required this.id, required this.title, required this.desc});

  PropertyDetail.fromJson(Map<String, dynamic> jsonMap) {
    this.id = int.parse(jsonMap['id']);
    this.title = jsonMap['title'];
    this.desc = jsonMap['description'];
  }
}

class PropertyProgress {
  late int id;
  late String title;
  late String dateTime;
  late String note;

  PropertyProgress(
      {required this.id,
      required this.title,
      required this.dateTime,
      required this.note});

  PropertyProgress.fromJson(Map<String, dynamic> jsonMap) {
    this.id = int.parse(jsonMap['id']);
    this.title = jsonMap['title'];
    this.dateTime = jsonMap['date'];
    this.note = jsonMap['note'];
  }
}
