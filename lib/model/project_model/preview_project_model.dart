part of '../models.dart';

class PreviewProjectModel {
  late int? id;
  late String? title;
  late String? projectType;
  late int? unitPrice;
  late double? targetFund;
  late double? currentFund;
  late bool? isFavorited;
  late Uint8List? cover;

  PreviewProjectModel(
      {this.id,
      this.title,
      this.projectType,
      this.unitPrice,
      this.targetFund,
      this.currentFund,
      this.isFavorited,
      this.cover});

  PreviewProjectModel.fromJson(Map<String, dynamic> jsonMap) {
    this.id = jsonMap['id'];
    this.title = jsonMap['title'];
    this.projectType = jsonMap['project_type']['name'];
    this.unitPrice = jsonMap['unit_price'];
    this.targetFund = double.parse(jsonMap['target_fund'].toString());
    this.currentFund = double.parse(jsonMap['current_fund'].toString());
    this.isFavorited = int.parse(jsonMap['is_favorited']) == 1 ? true : false;

    this.cover = base64.decode(jsonMap['cover']['data']);
  }

  PreviewProjectModel.fromJsonToDetail(Map<String, dynamic> jsonMap) {
    this.id = jsonMap['id'];
    this.title = jsonMap['title'];
    this.projectType = jsonMap['project_type']['name'];
    this.unitPrice = jsonMap['unit_price'];
    this.targetFund = double.parse(jsonMap['target_fund'].toString());
    this.currentFund = double.parse(jsonMap['current_fund'].toString());
    this.isFavorited = int.parse(jsonMap['is_favorited']) == 1 ? true : false;
  }
}
