part of '../models.dart';

class DeveloperModel {
  late int id;
  late String name;
  late int successProject;
  late Uint8List profilePicture;

  DeveloperModel(
      {required this.id,
      required this.name,
      required this.successProject,
      required this.profilePicture});

  DeveloperModel.fromJson(Map<String, dynamic> jsonMap) {
    this.id = int.parse(jsonMap['id']);
    this.name = jsonMap['name'];
    this.successProject = jsonMap['success_project'];
    this.profilePicture = base64.decode(jsonMap['profile_picture']);
  }
}
