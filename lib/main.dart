import 'package:flutter/material.dart';
import 'package:gdi_test/UI/pages/pages.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return MaterialApp(
          theme: ThemeData(primaryColor: Color(0xFF961114)),
          home: ListingPropertyPage(),
        );
      },
    );
  }
}
