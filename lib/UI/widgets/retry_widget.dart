part of 'widgets.dart';

class RetryWidget {
  static Widget retryWidget(BuildContext context, {required Function() ontap}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text('Terjadi Kesalahan!',
            style: TextStyle(fontSize: 12.0.sp, color: Colors.black54)),
        SizedBox(height: 1.0.h),
        GestureDetector(
          onTap: ontap,
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Theme.of(context).primaryColor),
              padding: EdgeInsets.symmetric(horizontal: 8.0.w, vertical: 4.0.w),
              child: Text('Coba lagi',
                  style: TextStyle(
                      fontSize: 12.0.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold))),
        )
      ],
    );
  }
}
