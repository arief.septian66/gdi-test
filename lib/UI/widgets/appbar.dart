part of 'widgets.dart';

class Appbar {
  static AppBar basicAppbar(
      {List<Widget>? actions, required String title, double? elevation}) {
    return AppBar(
      backgroundColor: Colors.white,
      titleSpacing: 0,
      elevation: elevation ?? 2,
      automaticallyImplyLeading: false,
      title: Row(
        children: [
          BackButton(
            color: Colors.black87,
          ),
          Text(title,
              style: TextStyle(
                  fontSize: 12.0.sp,
                  color: Colors.black87,
                  fontWeight: FontWeight.w800))
        ],
      ),
      actions: actions ?? [],
    );
  }
}
