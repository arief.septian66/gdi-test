part of 'widgets.dart';

class PlaceholderTemplate {
  static Widget placeHolderBasic(Widget widget) {
    return Shimmer.fromColors(
        baseColor: Colors.black12,
        highlightColor: Colors.white38,
        child: widget);
  }

  static detailPagePlaceholder() {
    return placeHolderBasic(SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 3.0.w),
        width: 94.0.w,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 3.0.w, bottom: 2.0.w),
              width: 94.0.w,
              height: 94.0.w * 0.75,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
            ),
            Row(
              children: [
                Container(
                  width: 20.0.w,
                  height: 5.0.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                ),
                Container(
                  margin: EdgeInsets.only(left: 2.0.w),
                  width: 15.0.w,
                  height: 5.0.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                )
              ],
            ),
            Container(
                margin: EdgeInsets.only(top: 3.0.w),
                width: 94.0.w,
                height: 4.0.w,
                color: Colors.white),
            Container(
                margin: EdgeInsets.only(top: 1.0.w),
                width: 94.0.w,
                height: 4.0.w,
                color: Colors.white),
            Container(
                margin: EdgeInsets.only(top: 1.0.w),
                width: 60.0.w,
                height: 4.0.w,
                color: Colors.white),
            Container(
                margin: EdgeInsets.only(top: 3.0.w),
                width: 40.0.w,
                height: 4.0.w,
                color: Colors.white),
            Container(
              margin: EdgeInsets.only(top: 3.0.w),
              width: 94.0.w,
              height: 20.0.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            Container(
                margin: EdgeInsets.only(top: 3.0.w),
                width: 94.0.w,
                height: 4.0.w,
                color: Colors.white),
            Container(
                margin: EdgeInsets.only(top: 1.0.w),
                width: 94.0.w,
                height: 4.0.w,
                color: Colors.white),
          ],
        ),
      ),
    ));
  }

  static listingPagePlaceholder() {
    return placeHolderBasic(Container(
      width: 44.0.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 44.0.w,
            height: 44.0.w * 0.75,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
          ),
          Container(
            margin: EdgeInsets.only(top: 2.0.w),
            child: Container(width: 44.0.w, height: 5.0.w, color: Colors.white),
          ),
          Container(
            margin: EdgeInsets.only(top: 1.0.w),
            child: Container(width: 25.0.w, height: 5.0.w, color: Colors.white),
          ),
          Container(
            margin: EdgeInsets.only(top: 2.0.w),
            child: Container(width: 40.0.w, height: 5.0.w, color: Colors.white),
          ),
        ],
      ),
    ));
  }
}
