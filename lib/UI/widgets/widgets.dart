import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

part 'appbar.dart';
part 'retry_widget.dart';
part 'placeholder_template.dart';
