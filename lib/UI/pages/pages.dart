import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gdi_test/UI/widgets/widgets.dart';
import 'package:gdi_test/cubits/cubits.dart';
import 'package:gdi_test/model/models.dart';
import 'package:gdi_test/shared/shared.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

//property screen
part 'property/detail_property_page.dart';
part 'property/listing_property_page.dart';
