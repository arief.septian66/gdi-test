part of '../pages.dart';

class ListingPropertyPage extends StatefulWidget {
  ListingPropertyPage({Key? key}) : super(key: key);

  @override
  _ListingPropertyPageState createState() => _ListingPropertyPageState();
}

class _ListingPropertyPageState extends State<ListingPropertyPage> {
  ScrollController scrollController = ScrollController();
  ProjectCubit previewCubit = ProjectCubit();

  @override
  void initState() {
    super.initState();
    previewCubit.loadProjectPreview();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _buildAppBar(),
      body: BlocBuilder<ProjectCubit, ProjectState>(
          bloc: previewCubit,
          builder: (context, state) {
            if (state is ProjectLoading) {
              return loadingWidget();
            } else if (state is ProjectPreviewLoaded) {
              return _buildContent(state.listDataProject);
            } else if (state is ProjectFailed) {
              return Container(
                width: 100.0.w,
                child: RetryWidget.retryWidget(context, ontap: () {
                  previewCubit.loadProjectPreview();
                }),
              );
            }
            return Container();
          }),
    );
  }

  Widget loadingWidget() {
    List<Widget> loadingWidget = [];
    for (var i = 0; i < 5; i++) {
      loadingWidget.add(PlaceholderTemplate.listingPagePlaceholder());
    }

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5.0.w, vertical: 5.0.w),
      child: Wrap(spacing: 2.0.w, runSpacing: 5.0.w, children: loadingWidget),
    );
  }

  AppBar _buildAppBar() {
    return Appbar.basicAppbar(
      title: 'Daftar Semua Proyek',
      actions: [
        GestureDetector(
          onTap: () {
            Fluttertoast.showToast(msg: 'Coming Soon');
          },
          child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.0.w),
              child: Icon(Icons.filter_alt_outlined, color: Colors.black87)),
        )
      ],
    );
  }

  Widget _buildContent(List<PreviewProjectModel> data) {
    Widget _buildDataCountTitle() {
      return Container(
          margin: EdgeInsets.all(5.0.w),
          child: Text(
              'Menampilkan ' +
                  data.length.toString() +
                  ' dari ' +
                  data.length.toString() +
                  ' proyek',
              style: TextStyle(
                fontSize: 12.0.sp,
                color: Colors.black38,
              )));
    }

    Widget _buildPropertyListingData() {
      List<Widget> listWidgetProperty = [];

      for (var i = 0; i < data.length; i++) {
        listWidgetProperty.add(InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => DetailPropertyPage(
                          id: data[i].id!.toString(),
                        )));
          },
          child: Container(
            width: 44.0.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                      width: 44.0.w,
                      height: 44.0.w * 0.75,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: MemoryImage(
                                data[i].cover!,
                              ))),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                          width: 44.0.w,
                          padding: EdgeInsets.all(2.0.w),
                          child: Row(
                            children: [
                              Container(
                                  width: 10.0.w,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Color(0xffE1e3e3)),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 1.0.w, horizontal: 0.5.w),
                                  child: Text(data[i].projectType!,
                                      style: TextStyle(fontSize: 8.0.sp))),
                              Stack(
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 1.0.w),
                                    width: 20.0.w,
                                    height: 5.0.w,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 1.0.w),
                                    width: 20.0.w *
                                        FunctionHelper
                                            .getPercentageOfTargetFund(
                                                data[i].currentFund!,
                                                data[i].targetFund!),
                                    height: 5.0.w,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Theme.of(context).primaryColor),
                                  ),
                                  Container(
                                    width: 20.0.w,
                                    height: 5.0.w,
                                    alignment: Alignment.center,
                                    child: Text(
                                      (FunctionHelper.getPercentageOfTargetFund(
                                                      data[i].currentFund!,
                                                      data[i].targetFund!) *
                                                  100)
                                              .toInt()
                                              .toString() +
                                          "% Progress",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 8.0.sp,
                                          color: Colors.black87),
                                    ),
                                  )
                                ],
                              ),
                              data[i].isFavorited!
                                  ? Container(
                                      width: 8.0.w,
                                      height: 5.0.w,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.red[300]),
                                      child: Icon(
                                        Icons.favorite,
                                        color: Theme.of(context).primaryColor,
                                        size: 4.0.w,
                                      ),
                                    )
                                  : Container(),
                            ],
                          )),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 2.0.w),
                  child: Text(
                    data[i].title!,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12.0.sp, color: Colors.black),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      FunctionHelper.moneyChanger(
                          double.parse(data[i].unitPrice!.toString())),
                      style: TextStyle(
                          fontSize: 10.0.sp,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' / unit saham',
                      style:
                          TextStyle(fontSize: 10.0.sp, color: Colors.black38),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
      }

      return Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0.w),
        width: 90.0.w,
        child: Wrap(
          spacing: 2.0.w,
          runSpacing: 5.0.w,
          children: listWidgetProperty,
        ),
      );
    }

    return SingleChildScrollView(
        controller: scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildDataCountTitle(),
            _buildPropertyListingData(),
          ],
        ));
  }
}
