part of '../pages.dart';

class DetailPropertyPage extends StatefulWidget {
  final String id;
  DetailPropertyPage({Key? key, required this.id}) : super(key: key);

  @override
  _DetailPropertyPageState createState() => _DetailPropertyPageState();
}

class _DetailPropertyPageState extends State<DetailPropertyPage> {
  ProjectCubit projectCubit = ProjectCubit();
  bool hideButton = false;
  int indexPreview = 0;
  bool loadedData = false;

  @override
  void initState() {
    super.initState();
    projectCubit.loadDetailProject(id: widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectCubit, ProjectState>(
        bloc: projectCubit,
        builder: (context, state) {
          if (state is ProjectLoading) {
            return Scaffold(
              appBar: _buildAppBar(),
              body: PlaceholderTemplate.detailPagePlaceholder(),
            );
          } else if (state is DetailProjectLoaded) {
            return Scaffold(
                appBar: _buildAppBar(),
                body: _buildContent(state.projectDetailData),
                bottomNavigationBar: _buildBottomNavBar());
          } else if (state is ProjectFailed) {
            return Scaffold(
              appBar: _buildAppBar(),
              body: RetryWidget.retryWidget(context, ontap: () {
                projectCubit.loadDetailProject(id: widget.id);
              }),
            );
          }
          return Container();
        });
  }

  Widget _buildBottomNavBar() {
    Widget buildBottomButton(
        {required String title, required String assetLogo}) {
      return GestureDetector(
        onTap: () {
          Fluttertoast.showToast(msg: 'Coming Soon');
        },
        child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(
                    color: Theme.of(context).primaryColor, width: 0.7.w)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: 3.5.h, width: 3.5.h, child: Image.asset(assetLogo)),
                SizedBox(width: 2.0.w),
                Text(title,
                    style: TextStyle(
                        fontSize: 13.0.sp,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold))
              ],
            )),
      );
    }

    return Container(
        width: double.infinity,
        height: 8.0.h,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 5.0,
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(vertical: 1.0.h, horizontal: 3.0.w),
        child: Row(
          children: [
            Container(
                width: 40.0.w,
                height: double.infinity,
                margin: EdgeInsets.only(right: 2.0.w),
                child: buildBottomButton(
                    title: 'Prospect', assetLogo: Values.prospectLogo)),
            Expanded(
              child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: buildBottomButton(
                      assetLogo: Values.logoPercentage,
                      title: 'Detil Bagi Hasil')),
            ),
          ],
        ));
  }

  Widget _buildContent(DetailProjectModel data) {
    List<Widget> listImageAndVideo = [];

    Widget buildPreview(
        {required Function() onTap,
        double? rightMargin,
        Widget? customWidget,
        required ImageProvider<Object> image}) {
      return GestureDetector(
        onTap: onTap,
        child: Container(
            width: 12.0.w,
            height: 12.0.w,
            margin: EdgeInsets.only(right: rightMargin ?? 2.0.w),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(4),
                image: DecorationImage(fit: BoxFit.cover, image: image),
                border: Border.all(color: Colors.white, width: 0.3.w)),
            alignment: Alignment.center,
            child: customWidget ?? Container()),
      );
    }

    for (var i = 0; i < data.listImages.length; i++) {
      listImageAndVideo.add(buildPreview(
        onTap: () {
          setState(() {
            indexPreview = i;
          });
        },
        image: MemoryImage(data.listImages[i]),
      ));
    }

    if (data.videosUrl != '') {
      listImageAndVideo.add(buildPreview(
          onTap: () {
            setState(() {
              indexPreview = data.listImages.length;
            });
          },
          customWidget: Icon(
            Icons.play_arrow,
            color: Colors.white,
            size: 6.0.w,
          ),
          image: NetworkImage('https://img.youtube.com/vi/' +
              FunctionHelper.getIdFromYoutubeUrl(data.videosUrl) +
              '/0.jpg')));
    }

    Widget _buildPreview() {
      return Stack(
        children: [
          indexPreview == data.listImages.length
              ? Container(
                  margin: EdgeInsets.only(top: 3.0.w, bottom: 2.0.w),
                  child: YoutubePlayer(
                    controller: YoutubePlayerController(
                      initialVideoId:
                          FunctionHelper.getIdFromYoutubeUrl(data.videosUrl),
                      flags: YoutubePlayerFlags(
                        hideControls: false,
                        controlsVisibleAtStart: false,
                        autoPlay: true,
                        mute: false,
                      ),
                    ),
                    showVideoProgressIndicator: true,
                    progressIndicatorColor: Theme.of(context).primaryColor,
                  ))
              : Container(
                  margin: EdgeInsets.only(top: 3.0.w, bottom: 2.0.w),
                  width: 94.0.w,
                  height: 94.0.w * 0.75,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black12,
                      image: DecorationImage(
                          image: MemoryImage(data.listImages[indexPreview]))),
                ),
          indexPreview == data.listImages.length
              ? Container()
              : Positioned(
                  bottom: 2.0.w,
                  child: Container(
                      padding: EdgeInsets.all(2.0.w),
                      width: 94.0.w,
                      child: Container(
                          width: double.infinity,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: listImageAndVideo))))
        ],
      );
    }

    Widget _buildBasicInformation() {
      Widget buildSummaryData({required String title, required String desc}) {
        return Flexible(
          flex: 1,
          child: Container(
              width: double.infinity,
              child: Column(
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        fontSize: 14.0.sp,
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 1.0.w),
                  Text(desc,
                      style: TextStyle(fontSize: 9.0.sp, color: Colors.black54))
                ],
              )),
        );
      }

      Widget buildCategoryData(
          {required String title, Color? backgroundColor, Color? titleColor}) {
        return Container(
            padding: EdgeInsets.symmetric(vertical: 1.0.w, horizontal: 3.0.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: backgroundColor ?? Colors.black12),
            child: Text(title,
                style: TextStyle(
                    fontSize: 10.0.sp,
                    color: titleColor ?? Colors.black54,
                    fontWeight: FontWeight.bold)));
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          indexPreview == data.listImages.length
              ? Container(
                  padding: EdgeInsets.all(2.0.w),
                  width: 94.0.w,
                  child: Container(
                      width: double.infinity,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: listImageAndVideo)))
              : Container(),
          Row(
            children: [
              buildCategoryData(
                title: data.previewProjectModel.projectType!,
              ),
              data.previewProjectModel.isFavorited!
                  ? Container(
                      margin: EdgeInsets.only(left: 2.0.w),
                      child: buildCategoryData(
                          title: 'Hot List',
                          backgroundColor: Color(0xfff7b0ad),
                          titleColor: Color(0xFF961114)),
                    )
                  : Container()
            ],
          ),
          SizedBox(height: 2.0.w),
          Text(
            data.previewProjectModel.title!,
            style: TextStyle(
                fontSize: 14.0.sp,
                color: Colors.black87,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 2.0.w),
          Row(
            children: [
              Text(
                FunctionHelper.moneyChanger(double.parse(
                    data.previewProjectModel.unitPrice.toString())),
                style: TextStyle(
                    fontSize: 14.0.sp,
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                ' / unit saham',
                style: TextStyle(fontSize: 14.0.sp, color: Colors.black38),
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(top: 4.0.w, bottom: 2.0.w),
              child: Card(
                  elevation: 4,
                  child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(
                          horizontal: 3.0.w, vertical: 2.0.w),
                      child: Row(
                        children: [
                          buildSummaryData(
                              title: data.bagiHasilMonth.toString() + " Bulan",
                              desc: 'Periode bagi hasil'),
                          buildSummaryData(
                              title: data.profitPerYear.toString() + "%",
                              desc: 'Profit / tahun'),
                          buildSummaryData(
                              title: data.isSellable ? 'Ya' : 'Tidak',
                              desc: 'Bisa Diperjualbelikan')
                        ],
                      )))),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Row(
                    children: [
                      Container(
                        width: 15.0.w,
                        height: 15.0.w,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: MemoryImage(
                                    data.developerModel.profilePicture))),
                      ),
                      SizedBox(
                        width: 3.0.w,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Proyek oleh',
                              style: TextStyle(
                                  fontSize: 12.0.sp, color: Colors.black45)),
                          Text(data.developerModel.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.0.sp,
                                  color: Colors.black87))
                        ],
                      )
                    ],
                  )
                ],
              ),
              data.developerModel.successProject == 0
                  ? Container()
                  : Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 1.0.w, horizontal: 2.0.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xFF4e9296)),
                      child: Text(
                        data.developerModel.successProject.toString() +
                            ' Proyek Sukses',
                        style: TextStyle(
                          fontSize: 10.0.sp,
                          color: Colors.white,
                        ),
                      ))
            ],
          )
        ],
      );
    }

    Widget _buildDetailInformation() {
      Widget titleWithWidget({required String title, required Widget widget}) {
        return Container(
          margin: EdgeInsets.only(top: 5.0.w),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 15.0.sp,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 3.0.w,
            ),
            widget
          ]),
        );
      }

      Widget buildPropertyDetailData(
          {required String title1, required String title2}) {
        return Container(
            width: 47.0.w,
            margin: EdgeInsets.only(bottom: 3.0.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title1,
                  style: TextStyle(fontSize: 12.0.sp, color: Colors.black54),
                ),
                Text(title2,
                    style: TextStyle(
                        fontSize: 13.0.sp,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold)),
              ],
            ));
      }

      Widget buildProgressData(
          {required bool isFirst,
          required String title,
          required String date,
          Widget? customLabel}) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      width: 3.0.w,
                      height: 3.0.w,
                      margin: EdgeInsets.only(right: 3.0.w),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 0.6.w, color: Colors.grey),
                          color: isFirst ? Colors.white : Colors.grey),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(title,
                            style: TextStyle(
                                fontSize: 12.0.sp,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold)),
                        Container(
                            margin: EdgeInsets.symmetric(vertical: 1.0.w),
                            child: Text(FunctionHelper.dateToReadable(date),
                                style: TextStyle(
                                  fontSize: 10.0.sp,
                                  color: Colors.black54,
                                )))
                      ],
                    )
                  ],
                ),
                customLabel ??
                    Container(
                        width: 6.0.w,
                        height: 6.0.w,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xFFb6dbdd)),
                        child: Icon(
                          Icons.check,
                          color: Color(0xFF2c5757),
                          size: 5.0.w,
                        ))
              ],
            ),
            Container(
                margin: EdgeInsets.only(left: 6.0.w, bottom: 3.0.w),
                width: double.infinity,
                height: 0.5.w,
                color: Colors.black12)
          ],
        );
      }

      Widget buildDescWidget() {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(data.description,
                maxLines: hideButton ? null : 5,
                style: TextStyle(fontSize: 12.0.sp, color: Colors.black87)),
            SizedBox(height: 3.0.w),
            GestureDetector(
              onTap: () {
                setState(() {
                  hideButton = !hideButton;
                });
              },
              child: Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: Text(hideButton ? 'Sembunyikan' : 'Selengkapnya',
                      style: TextStyle(
                          fontSize: 13.0.sp,
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold))),
            )
          ],
        );
      }

      //propertydetailwidget
      List<Widget> listPropertyDetailWidget = [];
      for (var i = 0; i < data.propertyDetail.length; i++) {
        listPropertyDetailWidget.add(buildPropertyDetailData(
            title1: data.propertyDetail[i].title,
            title2: data.propertyDetail[i].desc));
      }

      //Property progress Widget
      List<Widget> listPropertyProgressWidget = [];
      for (var i = data.propertyProgress.length - 1; i > 0; i--) {
        listPropertyProgressWidget.add(
          buildProgressData(
              title: data.propertyProgress[i].title,
              date: data.propertyProgress[i].dateTime,
              customLabel: data.propertyProgress[i].note == ''
                  ? null
                  : Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 1.0.w, horizontal: 2.0.w),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xFFf6deb8)),
                      child: Text(data.propertyProgress[i].note,
                          style: TextStyle(
                              fontSize: 10.0.sp,
                              color: Color(0xFFb2934b),
                              fontWeight: FontWeight.bold))),
              isFirst: i == data.propertyProgress.length - 1),
        );
      }

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleWithWidget(title: 'Deskripsi', widget: buildDescWidget()),
          titleWithWidget(
              title: 'Detil Properti',
              widget: Wrap(children: listPropertyDetailWidget)),
          titleWithWidget(
              title: 'Kemajuan Penerbit',
              widget: Column(children: listPropertyProgressWidget)),
          titleWithWidget(
              title: 'Lokasi Proyek',
              widget: Row(
                children: [
                  Flexible(
                    flex: 7,
                    child: Container(
                        width: double.infinity,
                        child: Text(
                          data.addressFull,
                          style: TextStyle(
                              fontSize: 11.0.sp, color: Colors.black54),
                        )),
                  ),
                  Flexible(
                    flex: 3,
                    child: GestureDetector(
                      onTap: () async {
                        String url =
                            'https://www.google.com/maps/search/?api=1&query=' +
                                data.latitude +
                                "," +
                                data.longitude;
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          Fluttertoast.showToast(
                              msg: 'Tidak Dapat Membuka Maps');
                        }
                      },
                      child: Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            'Lihat Peta',
                            style: TextStyle(
                                fontSize: 13.0.sp,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                  )
                ],
              )),
          SizedBox(height: 5.0.h)
        ],
      );
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 3.0.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildPreview(),
            _buildBasicInformation(),
            _buildDetailInformation()
          ],
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return Appbar.basicAppbar(
      title: 'Detil Proyek',
      actions: [
        GestureDetector(
          onTap: () {
            Fluttertoast.showToast(msg: 'Coming Soon');
          },
          child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.0.w),
              child: Icon(Icons.more_vert_rounded, color: Colors.black87)),
        )
      ],
    );
  }
}
