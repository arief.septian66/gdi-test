part of 'shared.dart';

class FunctionHelper {
  static String moneyChanger(double value) {
    return NumberFormat.currency(name: 'Rp', decimalDigits: 0, locale: 'id')
        .format(value.round());
  }

  static double getPercentageOfTargetFund(
      double currentFund, double targetFund) {
    double returnValue = currentFund / targetFund;

    return returnValue > 1 ? 1 : returnValue;
  }

  static String dateToReadable(String date) {
    String finalString = '';

    List<String> breakDate = date.split('-');

    finalString = breakDate[2] + " ";

    switch (breakDate[1]) {
      case '01':
        finalString = finalString + 'Januari';
        break;
      case '02':
        finalString = finalString + 'Februari';
        break;
      case '03':
        finalString = finalString + 'Maret';
        break;
      case '04':
        finalString = finalString + 'April';
        break;
      case '05':
        finalString = finalString + 'Mei';
        break;
      case '06':
        finalString = finalString + 'Juni';
        break;
      case '07':
        finalString = finalString + 'Juli';
        break;
      case '08':
        finalString = finalString + 'Agustus';
        break;
      case '09':
        finalString = finalString + 'september';
        break;
      case '10':
        finalString = finalString + 'Oktober';
        break;
      case '11':
        finalString = finalString + 'November';
        break;
      case '12':
        finalString = finalString + 'Desember';
        break;
      default:
    }

    finalString = finalString + " " + breakDate[0];

    return finalString;
  }

  static String getIdFromYoutubeUrl(String url) {
    Uri urlUri = Uri.parse(url);
    Map<String, String> param = urlUri.queryParameters;
    return param['v']!;
  }

  static Color getColoByPercentage(double percentage) {
    if (percentage <= 0.5) {
      return Colors.red;
    } else if (percentage <= 0.8) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }
}
