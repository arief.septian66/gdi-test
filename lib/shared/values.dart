part of 'shared.dart';

class Values {
  static final logoPercentage = 'assets/images/percentage_logo.png';
  static final prospectLogo = 'assets/images/prospect_logo.png';
}

final String baseUrl = 'https://bellfashion.my.id/API/test_gdi/';

enum RequestStatus {
  success_request,
  failed_request,
  server_error,
  failed_parsing,
  internet_error
}
