part of '../services.dart';

class ProjectAPI {
  static Future<ApiReturnValue<List<PreviewProjectModel>>>
      listingPreviewProject() async {
    ApiReturnValue<List<PreviewProjectModel>> returnValue;

    var request = http.MultipartRequest(
        'POST', Uri.parse(baseUrl + 'listing_project.php'));

    ApiReturnValue<dynamic>? httpRequestData =
        await ApiReturnValue.httpRequest(request: request);

    if (httpRequestData!.status == RequestStatus.success_request) {
      final dataRaw = httpRequestData.data;

      if (dataRaw['status']) {
        List<dynamic> listDataRaw = dataRaw['data'];
        List<PreviewProjectModel> dataFinal = [];
        for (var i = 0; i < listDataRaw.length; i++) {
          dataFinal.add(PreviewProjectModel.fromJson(listDataRaw[i]));
        }

        returnValue = ApiReturnValue(
            data: dataFinal, status: RequestStatus.success_request);
      } else {
        returnValue = ApiReturnValue(status: RequestStatus.failed_request);
      }
    } else {
      returnValue = ApiReturnValue(status: httpRequestData.status);
    }

    return returnValue;
  }

  static Future<ApiReturnValue<DetailProjectModel>> detailProject(
      {required String id}) async {
    ApiReturnValue<DetailProjectModel> returnValue;

    var request = http.MultipartRequest(
        'POST', Uri.parse(baseUrl + 'detail_project.php'));

    request.fields['id'] = id;

    ApiReturnValue<dynamic>? httpRequestData =
        await ApiReturnValue.httpRequest(request: request);

    if (httpRequestData!.status == RequestStatus.success_request) {
      final dataRaw = httpRequestData.data;

      if (dataRaw['status']) {
        dynamic dataRawFinal = dataRaw['data'];

        returnValue = ApiReturnValue(
            data: DetailProjectModel.fromJson(dataRawFinal),
            status: RequestStatus.success_request);
      } else {
        returnValue = ApiReturnValue(status: RequestStatus.failed_request);
      }
    } else {
      returnValue = ApiReturnValue(status: httpRequestData.status);
    }

    return returnValue;
  }
}
